package Models;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import org.bson.Document;

public class Manage_Data {

	private String API_key="4f84e7bf4319eda827f89b7843d3a74d";
	private String format="json";
	
	public Document getTrackInfo(String data_recup, String method, HashMap<String,String> params, boolean insertBddLocal) {
		//searchDataLocal() //a appeler avant
		try {
			String parameters="";
			for (Map.Entry m : params.entrySet()) {
				parameters+=m.getKey();
				parameters+="=";
				parameters+=URLEncoder.encode(m.getValue().toString(), "UTF-8");
				parameters+="&";
	        }
			if(parameters.equals("")) {
				System.out.println("Saise des paramètres obligatoires");
				return null;
			}
			
			
			String url = "https://ws.audioscrobbler.com/2.0/?method="
					+data_recup
					+"."+ method + "&" +
					parameters+
					"&api_key="+ API_key+
					"&format="+format;
			//System.out.println(url.toString());
			HTTPTools httpTools = new HTTPTools();
			String jsonResponse = httpTools.sendGet(url);
			Document docLastFm = Document.parse(jsonResponse);
			// Création du JSON à retourner
			//Document respDoc = new Document();
			// Extraction de données de docLastFm et insertion dans respDoc
			// voir l’API org.bson.Document
			if(insertBddLocal)
				insertDataLocal();
				
			return docLastFm;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
		//TODO insertion base de données local si elles n'y sont pas 
	}
	
	public void searchDataLocal() {
		
	}
	
	public void insertDataLocal() {
		//on insère que le contenu de album, artist et tag getinfo, 
	}
}
